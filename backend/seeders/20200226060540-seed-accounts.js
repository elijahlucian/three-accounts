'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Accounts',
      [
        {
          id: 'kande',
          name: 'Kaela & Elijah',
          details: 'Household Accounts',
          crap: 1000,
          groceries: 800,
          takeout: 500,
          income: 5248,
          expenses: 1894,
          createdAt: new Date().toDateString(),
          updatedAt: new Date().toDateString(),
        },
      ],
      {}
    )
  },

  down: (queryInterface, equelize) => {
    return queryInterface.bulkDelete('Accounts', null, {})
  },
}
