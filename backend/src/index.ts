// @ts-nocheck
const express = require('express')
const db = require('../models')

class App {
  server: any
  db: any
  orm: any
  models: any

  constructor() {
    this.server = express()
    this.server.use(express.static(__dirname + '/../public'))
    this.server.use(express.urlencoded({ extended: true }))
    this.server.use(express.json())
    console.log('db init')
    this.db = db
    this.init()
  }

  init() {
    this.server.post(
      '/api/login',
      async ({ body: { name, password } }, res) => {
        if (!name || !password) {
          res.status(422)
          res.send({ message: 'you must supply a username and password!' })
          return
        }

        const record = await this.db.User.findOne({ where: { name } })
        if (record?.password === password) {
          res.send({ message: 'SUCK SEEDED!', goldenTicket: 42 })
        } else {
          res.status(403)
          res.send({ message: "Ain't the right password, dog!" })
        }
      }
    )
    this.server.get('/api/users/:id?', async ({ params: { id } }, res) => {
      const q = id
        ? await this.db.User.findOne({
            where: { id },
            include: ['accounts'],
          })
        : await this.db.User.findAll({ include: ['accounts'] })
      this.handleQuery(q, res)
    })

    this.server.get('/api/accounts/:id?', async ({ params: { id } }, res) => {
      const q = id
        ? await this.db.Account.findOne({ where: { id } })
        : await this.db.Account.findAll()
      this.handleQuery(q, res)
    })

    this.server.get(
      '/api/accounts/:id/transactions/:year?/:month?',
      async ({ params: { id } }, res) => {
        const q = await this.db.Transaction.findAll({
          where: { accountId: id },
        })

        // TODO: where year and month are in this range.
        this.handleQueryAll(q, res)
      }
    )
    this.server.post(
      '/api/accounts/:id/transactions',
      async ({ params: { id }, body }, res) => {
        console.log('new tx =>', id, body)

        this.db.Transaction.create({ ...body, accountId: id })
        res.send({ body, id })
      }
    )
  }

  handleQuery(record, res) {
    if (record) {
      res.send(record)
    } else this.notFound(res)
  }

  handleQueryAll(records, res) {
    if (records.length) {
      res.send(records)
    } else this.notFound(res)
  }

  notFound(res) {
    res.send({ message: 'not found' })
  }

  start() {
    this.server.listen(4242)
    console.log('listening on 4242!')
  }
}

const app = new App()
app.start()
