'use strict'

module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define(
    'Account',
    {
      name: DataTypes.STRING,
      details: DataTypes.STRING,
      crap: DataTypes.NUMBER,
      groceries: DataTypes.NUMBER,
      takeout: DataTypes.NUMBER,
      income: DataTypes.NUMBER, // HOUSEHOLD INCOME
      expenses: DataTypes.NUMBER, // MONTHLY EXPENSES
    },
    {}
  )
  Account.associate = models => {
    Account.hasMany(models.Transaction)
    Account.belongsToMany(models.User, { through: 'UserAccounts' })
    // associations can be defined here
  }
  return Account
}
