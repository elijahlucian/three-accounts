'use strict'
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {}
  )
  User.associate = models => {
    User.belongsToMany(models.Account, {
      through: 'UserAccounts',
      as: 'accounts',
      foreignKey: 'userId',
    })
    // User.hasMany(models.accounts)
    // associations can be defined here
  }
  return User
}
