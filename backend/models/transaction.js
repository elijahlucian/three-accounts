'use strict'
module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define(
    'Transaction',
    {
      accountId: DataTypes.STRING,
      accountName: DataTypes.STRING, // TODO: rename to 'type' or 'bucket' or 'wallet'
      amount: DataTypes.NUMBER,
      details: DataTypes.STRING,
    },
    {}
  )
  Transaction.associate = function(models) {
    // associations can be defined here
  }
  return Transaction
}
