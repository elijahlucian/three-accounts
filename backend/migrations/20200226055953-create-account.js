'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Accounts', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      name: {
        type: Sequelize.STRING,
      },
      details: {
        type: Sequelize.STRING,
      },
      groceries: {
        type: Sequelize.NUMBER,
      },
      takeout: {
        type: Sequelize.NUMBER,
      },
      crap: {
        type: Sequelize.NUMBER,
      },
      income: {
        type: Sequelize.NUMBER,
      },
      expenses: {
        type: Sequelize.NUMBER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Accounts')
  },
}
