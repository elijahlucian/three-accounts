import React, { useState, SetStateAction, Dispatch, useEffect } from 'react'
import {
  Profile,
  AddTX,
  Transaction,
  CalculatedAccountIndex,
  User,
} from './types'
import axios from 'axios'
import { txIndex, profileIndex } from './data/db'

type Props = {
  user: User
  children: any
}

type Context = {
  user: User
  profile?: Profile
  addTransaction: AddTX
  error?: string
  setError: Dispatch<SetStateAction<string | undefined>>
  selectProfile: (account: string) => void
  balances?: CalculatedAccountIndex
  loaded: boolean
}

export const AppContext = React.createContext<Context | null>(null)

export const AppContextProvider = ({ children, user }: Props) => {
  const [error, setError] = useState<string>()
  const [txs, setTxs] = useState<Transaction[]>([])
  const [balances, setBalances] = useState<CalculatedAccountIndex>()
  // choice of User.accounts
  const [selectedProfile, setSelectedProfile] = useState<string>()
  const [profile, setProfile] = useState<Profile>()
  const [loaded, setLoaded] = useState(false)

  // Check if PROFILE is set up, if not do new user workflow

  useEffect(() => {
    window.localStorage.user = user.id
    // const get = async () => {
    //   if (!user) return
    //   if (process.env.NODE_ENV === 'development') {
    //     console.log('Dev > Setting profile to:', user.id)
    //     setProfile(profileIndex[user.id])
    //   }
    // }
    // get()
  }, [user])

  useEffect(() => {
    const get = async () => {
      if (!selectedProfile) return
      // set profile then transactionsIndex.

      if (process.env.NODE_ENV === 'development') {
        setProfile(profileIndex[selectedProfile])

        console.log(
          'Dev > Getting Mock Transactions:',
          txIndex[selectedProfile]
        )
        setTxs(txIndex[selectedProfile])
        return
      }
      const { data: profile } = await axios.get(
        `/api/accounts/${selectedProfile}`
      )
      setProfile(profile)

      const { data: transactions } = await axios.get(
        `/api/accounts/${selectedProfile}/transactions`
      )
      console.log('Getting Transactions', transactions)
      setTxs(transactions)
    }
    get()
  }, [selectedProfile])

  useEffect(() => {
    // API get transactions
    const balances: CalculatedAccountIndex = {
      crap: [],
      groceries: [],
      takeout: [],
    }
    txs.forEach((tx: Transaction) => {
      balances[tx.accountName].push(tx)
    })
    setBalances(balances)
    setLoaded(true)
  }, [txs])

  const selectProfile = (account: string) => {
    console.log('selecting profile', account)
    setSelectedProfile(account)
  }

  const addTransaction = async (tx: Transaction) => {
    if (!user) return
    console.log('adding TX', tx.accountName, tx)
    const { data } = await axios.post(
      `/api/accounts/${user?.id}/transactions`,
      tx
    )
    console.log('Added Transaction =>', data)
    // API Insert transaction
    const newtxs = [...txs, tx]
    setTxs(newtxs)
  }

  return (
    <AppContext.Provider
      value={{
        balances,
        user,
        profile,
        addTransaction,
        error,
        setError,
        selectProfile,
        loaded, // User and Profile both loaded
      }}
    >
      {children}
    </AppContext.Provider>
  )
}
