export type AccountType = 'groceries' | 'takeout' | 'crap'

export type CalculatedAccountIndex = Record<AccountType, Transaction[]>

export type AccountIndex = Record<AccountType, number>

export type Transaction = {
  // Q: where account_id = user_id, and time between month.start & month.end
  amount: number
  details: string
  accountName: AccountType
  accountId?: string
}

export type Profile = AccountIndex & {
  id: string
  name: string
  // transactions: Transaction[]
}

export type AddTX = (tx: Transaction) => void

export type User = {
  id: string
  name: string
  accounts: Profile[]
}
