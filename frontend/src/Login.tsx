import React, { useState } from 'react'
import Axios from 'axios'

type Props = {
  handleLogin: (v: string) => void
}

export const Login = ({ handleLogin }: Props) => {
  const [name, setName] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    if (process.env.NODE_ENV === 'development') {
      console.log('DEV > Logging in with user', name)
      handleLogin(name)
      return
    }
    if (!name || !password) {
      setError('please fill out both fields')
      return
    }
    try {
      const { data } = await Axios.post('/api/login', { name, password })
      console.log('login successful...', data)
      handleLogin(name)
    } catch (err) {
      setError(err.message)
    }
    // if success
    // write name to local storage
  }

  return (
    <div className="login">
      <form onSubmit={handleSubmit}>
        <label>
          Type Yo Name:
          <input
            autoFocus
            onChange={e => setName(e.target.value)}
            value={name}
          ></input>
        </label>
        <label>
          Type Yo Password:
          <input
            autoFocus
            onChange={e => setPassword(e.target.value)}
            value={password}
          ></input>
        </label>
        <label>
          <button type="submit">Submit</button>
        </label>
        <p className="error">{error}</p>
      </form>
    </div>
  )
}
