import { Profile, Transaction, User } from '../types'

export const profileIndex: Record<string, Profile> = {
  kande: {
    id: 'kande',
    name: 'Kaela & Elijah',
    groceries: 800,
    crap: 1000,
    takeout: 500,
  },
}

export const userIndex: Record<string, User> = {
  elijah: { id: 'elijah', name: 'Elijah', accounts: [profileIndex.kande] },
  kaela: { id: 'kaela', name: 'Kaela', accounts: [profileIndex.kande] },
}

export const txIndex: Record<string, Transaction[]> = {
  kande: [
    { accountName: 'groceries', amount: 400, details: 'halfmonth' },
    { accountName: 'groceries', amount: 122, details: 'superstore' },
    { accountName: 'groceries', amount: 21.71, details: 'community' },
    { accountName: 'groceries', amount: 75.08, details: 'sobeys' },
    { accountName: 'groceries', amount: 32.56, details: 'sobeys' },
    { accountName: 'crap', amount: 500, details: 'halfmonth' },
    { accountName: 'crap', amount: 149.09, details: 'vechicle' },
    { accountName: 'crap', amount: 20, details: 'starbucks' },
    { accountName: 'crap', amount: 20.81, details: 'thriftstore' },
    { accountName: 'crap', amount: 47, details: 'gas rav4' },
    { accountName: 'crap', amount: 40, details: 'gas van' },
    { accountName: 'takeout', amount: 250, details: 'halfmonth' },
    { accountName: 'takeout', amount: 47.12, details: 'raj palace' },
    { accountName: 'takeout', amount: 14.66, details: '' },
    { accountName: 'takeout', amount: 18.48, details: 'safari grill' },
    { accountName: 'takeout', amount: 72.82, details: 'Li Ao' },
    { accountName: 'takeout', amount: 13.58, details: 'Thai Express' },
  ],
}
