import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import { App } from './App'
import { Login } from './Login'
import { AppContextProvider } from './AppContext'
import { userIndex } from './data/db'
import * as serviceWorker from './serviceWorker'
import './index.css'
import { AccountType, User } from './types'
import { TransactionContextProvider } from './TransactionContext'

import axios from 'axios'

export const accountTypes: AccountType[] = ['groceries', 'takeout', 'crap']

const AppWithContext = () => {
  const [user, setUser] = useState<User>()

  useEffect(() => {
    if (window.localStorage.user) setUser(userIndex[window.localStorage.user])
  }, [])

  const handleLogin = async (name: string) => {
    // GET USER PROFILE with joined accounts

    if (process.env.NODE_ENV === 'development') {
      console.log('DEV > logging into mock', name, userIndex[name])
      setUser(userIndex[name])
      return
    }
    // GET USER
    // GET ACCOUNT
    const { data } = await axios.get(`/api/users/${name}/`)
    console.log('logging into', name, data)
    setUser(data)
  }

  return !user ? (
    <>
      <Login handleLogin={handleLogin} />
    </>
  ) : (
    <AppContextProvider user={user}>
      <TransactionContextProvider>
        <App />
      </TransactionContextProvider>
    </AppContextProvider>
  )
}

ReactDOM.render(<AppWithContext />, document.getElementById('root'))
serviceWorker.unregister()
