import React, { useContext } from 'react'
import { AppContext } from './AppContext'
import { AccountType } from './types'
import { TransactionContext } from './TransactionContext'

type Props = {
  label: string
  col: number
  account: AccountType
}

export const FundBar = ({ label, col, account }: Props) => {
  const appContext = useContext(AppContext)
  const { toggleModal } = useContext(TransactionContext)

  if (
    !appContext ||
    !appContext?.profile ||
    !appContext?.balances ||
    !appContext.balances[account].length
  )
    return <div>Loading</div>

  const { amount } = appContext.balances[account].reduce(
    ({ amount: prevAmount, ...prev }, { amount, ...curr }) => {
      return { ...curr, amount: prevAmount + amount }
    }
  )

  const max = appContext?.profile[account]
  const current = max - amount
  const percent = current / max
  const hue = percent * 120

  return (
    <>
      <div
        className={`fundbar back col${col}`}
        onClick={() => toggleModal(account)}
      ></div>
      <div
        className={`fundbar front col${col}`}
        style={{
          background: `hsl(${hue}, 100%, 50%)`,
          height: `${percent * 40 + 10}vmin`,
        }}
      >
        <h3>{label}</h3>
      </div>
      <div
        className={`fundbar totals col${col}  ${
          percent > 0.5 ? 'below' : 'above'
        }`}
      >
        ${Math.floor(current)} / ${max}
      </div>
    </>
  )
}
