import React, { useContext } from 'react'
import { AppContext } from './AppContext'
import { accountTypes } from './'
import { TransactionDetail } from './TransactionDetail'

export const TransactionList = () => {
  const appContext = useContext(AppContext)
  if (!appContext?.loaded) return <div>asdf</div>

  return (
    <div className="funds">
      {accountTypes.map((account, i) => {
        return (
          <div key={`${account}-txs-${i}`}>
            <h3>{account}</h3>
            {appContext.balances &&
              appContext.balances[account].map(tx => (
                <TransactionDetail transaction={tx} />
              ))}
          </div>
        )
      })}
    </div>
  )
}
