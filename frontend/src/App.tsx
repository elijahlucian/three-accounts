import React, { useContext, useState } from 'react'
// import io from 'socket.io-client'
import { FundBar } from './FundBar'
import { TransactionList } from './TransactionList'
import './App.scss'
import { TransactionModal } from './TransactionModal'
import { TransactionContext } from './TransactionContext'
import { AppContext } from './AppContext'

export const App = () => {
  const [showDetails, setShowDetails] = useState(false)
  const appContext = useContext(AppContext)
  const { account, showingModal } = useContext(TransactionContext)

  if (!appContext?.user) return <div>something went wrong!</div>

  const { user, profile, selectProfile } = appContext
  if (!profile)
    return (
      <div className="app" id="appElement">
        <div>select budget profile!</div>
        {user.accounts.map(profile => (
          <button
            key={`profile-${profile.name}`}
            onClick={() => selectProfile(profile.id)}
          >
            {profile.name}
          </button>
        ))}
      </div>
    )

  return (
    <div className="app" id="appElement">
      <nav>
        <button onClick={() => setShowDetails(!showDetails)}>
          Toggle Details
        </button>
        <button onClick={() => delete window.localStorage.user}>Nuke</button>
      </nav>

      {showDetails ? (
        <TransactionList />
      ) : (
        <>
          <h1>Remaining Balances</h1>
          <div className="funds">
            <FundBar account="groceries" label="Groceries" col={1} />
            <FundBar account="takeout" label="Takeout" col={2} />
            <FundBar account="crap" label="Crap" col={3} />
          </div>
          {showingModal && <TransactionModal account={account} />}
        </>
      )}
    </div>
  )
}
