import React from 'react'
import { Transaction } from './types'

type Props = {
  transaction: Transaction
}

export const TransactionDetail = ({ transaction }: Props) => {
  return (
    <p>
      ${transaction.amount} - {transaction.details}
    </p>
  )
}
